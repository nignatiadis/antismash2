#ifndef _DELETEREPEAT_H_
#define _DELETEREPEAT_H_

#include "QueueClass.h"

void deleteRepeat( QueueClass< UnitClass> &peptidelibrary);
string reverseSMILES( const string &forSMILES);

#include "deleteRepeat.inl"
#endif

