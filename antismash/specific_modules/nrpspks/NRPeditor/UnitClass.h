#ifndef _UNITCLASS_H_
#define _UNITCLASS_H_

#include "constants.h" 

class UnitClass
{
  private:
  string unit;              // amino acid, or other substrate
  string smileString; 
  string formula;
  string sequence;
  float mass; 
  int startSite;
  int endSite;

  public:

    bool operator< ( const UnitClass & rhs ) const;
    bool operator<= ( const float rhs ) const;
    bool operator> ( const float rhs ) const;
    bool operator>= ( const float rhs ) const;

    void setUnit( const string inUnit, const string inSmile );

    void setSmileString( const string inSmile );

    void setMass( const float inMass );
 
    void setStartSite( const int inSite);

    void setEndSite( const int inSite);

    void setFormula( const string inFormula );

    void setSequence( const string inSequence );
   
    string getUnit( ) const;

    string getSmile( ) const;

    float getMass( ) const;

    int getStartSite( ) const;

    int getEndSite( ) const;

    string getFormula( ) const;

    string getSequence( ) const;

};

#include "UnitClass.inl"
#endif
