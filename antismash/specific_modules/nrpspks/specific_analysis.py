# vim: set fileencoding=utf-8 :
#
# Copyright (C) 2010-2012 Marnix H. Medema
# University of Groningen
# Department of Microbial Physiology / Groningen Bioinformatics Centre
#
# Copyright (C) 2011,2012 Kai Blin
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Div. of Microbiology/Biotechnology
#
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.
'''
In-depth analysis and annotation of NRPS/PKS gene clusters.
'''

import os
import logging
from os import path
from antismash import utils
from nrpspksdomains import annotate_pksnrps
from substrates import run_nrps_substr_spec_predictions, run_pks_substr_spec_predictions, parse_substr_spec_predictions
from parsers import calculate_consensus_prediction, generate_domainnamesdict, write_substr_spec_preds_to_html
from orderfinder import analyse_biosynthetic_order
from structure_drawer import generate_chemical_structure_preds

def create_rawoutput_storage_folder(options):
    #Make output folder for storing raw predictions
    options.raw_predictions_outputfolder = path.abspath(path.join(options.outputfoldername, "nrpspks_predictions_txt"))
    if not os.path.exists(options.raw_predictions_outputfolder):
        os.mkdir(options.raw_predictions_outputfolder)

def create_pksnrpsvars_object():
    pksnrpsvars = utils.Storage()
    pksnrpsvars.nrpspkstypedict = {}
    pksnrpsvars.compound_pred_dict = {}
    pksnrpsvars.domaindict = {}
    pksnrpsvars.failedstructures = []
    pksnrpsvars.dockingdomainanalysis = []
    pksnrpsvars.pksnrpscoregenes = []
    return pksnrpsvars

def specific_analysis(seq_record, options):
    pksnrpsvars = create_pksnrpsvars_object()
    create_rawoutput_storage_folder(options)
    pksnrpsvars = annotate_pksnrps(pksnrpsvars, seq_record)
    if len(pksnrpsvars.pksnrpscoregenes) > 0:
        run_nrps_substr_spec_predictions(pksnrpsvars, seq_record, options)
        run_pks_substr_spec_predictions(pksnrpsvars, seq_record, options)
        parse_substr_spec_predictions(pksnrpsvars, options)
        calculate_consensus_prediction(pksnrpsvars)
        generate_domainnamesdict(pksnrpsvars)
        analyse_biosynthetic_order(pksnrpsvars, seq_record, options)
    generate_chemical_structure_preds(pksnrpsvars, seq_record, options)
    write_substr_spec_preds_to_html(options, pksnrpsvars)
    write_data_to_seq_record(pksnrpsvars, seq_record, options)

def write_data_to_seq_record(pksnrpsvars, seq_record, options):
    #Save substrate specificity predictions in NRPS/PKS domain sec_met info of seq_record
    for feature in pksnrpsvars.pksnrpscoregenes:
        nrat = 0
        nra = 0
        nrcal = 0
        nrkr = 0
        secmetqualifiers = feature.qualifiers['sec_met']
        updated_secmetqualifiers = []
        gene_id = utils.get_gene_id(feature)
        for qualifier in secmetqualifiers:
            if "NRPS/PKS Domain:" not in qualifier:
                updated_secmetqualifiers.append(qualifier)
            else:
                if "AMP-binding" in qualifier:
                    nra += 1
                    domainname = gene_id + "_A" + str(nra)
                    newqualifier = qualifier + " Substrate specificity predictions: %s (NRPSPredictor2 SVM), %s (Stachelhaus code), %s (Minowa), %s (consensus);" %(pksnrpsvars.nrps_svm_preds[domainname], pksnrpsvars.nrps_code_preds[domainname], pksnrpsvars.minowa_nrps_preds[domainname], pksnrpsvars.consensuspreds[domainname])
                    updated_secmetqualifiers.append(newqualifier)
                elif "PKS_AT" in qualifier:
                    nrat += 1
                    domainname = gene_id + "_AT" + str(nrat)
                    newqualifier = qualifier + " Substrate specificity predictions: %s (PKS signature), %s (Minowa), %s (consensus);" %(pksnrpsvars.pks_code_preds[domainname], pksnrpsvars.minowa_pks_preds[domainname], pksnrpsvars.consensuspreds[domainname])
                    updated_secmetqualifiers.append(newqualifier)
                elif "CAL_domain" in qualifier:
                    nrcal += 1
                    domainname = gene_id + "_CAL" + str(nrcal)
                    newqualifier = qualifier + " Substrate specificity predictions: %s (Minowa);" %(pksnrpsvars.minowa_cal_preds[domainname])
                    updated_secmetqualifiers.append(newqualifier)
                elif "PKS_KR" in qualifier:
                    nrkr += 1
                    domainname = gene_id + "_KR" + str(nrkr)
                    newqualifier = qualifier + " Predicted KR activity: %s; Predicted KR stereochemistry: %s;" %(pksnrpsvars.kr_activity_preds[domainname], pksnrpsvars.kr_stereo_preds[domainname])
                    updated_secmetqualifiers.append(newqualifier)
                else:
                    updated_secmetqualifiers.append(qualifier)
        feature.qualifiers['sec_met'] = updated_secmetqualifiers
    #Save consensus structure + link to structure image to seq_record
    clusters = utils.get_cluster_features(seq_record)
    for cluster in clusters:
        clusternr = utils.get_cluster_number(cluster)
        if pksnrpsvars.compound_pred_dict.has_key(clusternr):
            structpred = pksnrpsvars.compound_pred_dict[clusternr]
            cluster.qualifiers['note'].append("Monomers prediction: " + structpred)
            cluster.qualifiers['note'].append("Structure image: structures/genecluster%s.png" % clusternr)
